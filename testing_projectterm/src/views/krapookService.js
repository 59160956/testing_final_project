const krapookService = {
  moneyWithdraw: function (moneyUser, moneyFrom) {
    return moneyUser >= moneyFrom
  },
  moneyCheckString: function (money) {
    if (money === null) {
      return false
    }
    if (isNaN(money)) {
      return false
    }
    if (money === 0) {
      return false
    }
    if (money < 0) {
      return false
    }
    return true
  }
}

export default krapookService
