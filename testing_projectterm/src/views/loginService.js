const loginService = {
  checkNull: function (data) {
    return data === null
  },
  checkNullUsernameAndPasseord: function (username, password) {
    return username.length < 1 || password.length < 1
  },
  checkTrueUsernameAndPasseord: function (username, password) {
    return username !== 'admin001' || password !== 'admin001'
  }
}

export default loginService
