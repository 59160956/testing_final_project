/* eslint-disable no-undef */
import loginService from '../../src/views/loginService'
import krapookService from '../../src/views/krapookService'
// eslint-disable-next-line no-undef
describe('Login เข้าระบบ', () => {
  it('Login เข้าสู่ระบบได้ถูกต้อง', () => {
    const data = {
      _id: '5e8c515140c2db3274da2985',
      firstname: 'thee',
      lastname: 'nugu',
      username: 'admin001',
      password: 'admin001',
      __v: 0
    }
    const login = loginService.checkNull(data)
    expect(login).toBe(false)
  })
  it('Login เข้าสู่ระบบไม่ได้ Username และ Password ไม่ถูกต้อง', () => {
    const username = 'admin'
    const password = 'admin'
    const login = loginService.checkTrueUsernameAndPasseord(username, password)
    expect(login).toBe(true)
  })
  it('Login เข้าสู่ระบบไม่ได้ Username : null', () => {
    const username = ''
    const password = 'admin'
    const login = loginService.checkNullUsernameAndPasseord(username, password)
    expect(login).toBe(true)
  })
  it('Login เข้าสู่ระบบไม่ได้ Password: null', () => {
    const username = 'admin'
    const password = ''
    const login = loginService.checkNullUsernameAndPasseord(username, password)
    expect(login).toBe(true)
  })
  it('Login เข้าสู่ระบบไม่ได้ Username : null และ Password : null', () => {
    const username = ''
    const password = ''
    const login = loginService.checkNullUsernameAndPasseord(username, password)
    expect(login).toBe(true)
  })
  it('Login เข้าสู่ระบบไม่ได้ Username : True และ Password : False', () => {
    const username = 'admin001'
    const password = 'admin'
    const login = loginService.checkTrueUsernameAndPasseord(username, password)
    expect(login).toBe(true)
  })
  it('Login เข้าสู่ระบบไม่ได้ Username : False และ Password : True', () => {
    const username = 'admin'
    const password = 'admin001'
    const login = loginService.checkTrueUsernameAndPasseord(username, password)
    expect(login).toBe(true)
  })
  it('login return data null', () => {
    const data = null
    const login = loginService.checkNull(data)
    expect(login).toBe(true)
  })
})
describe('ฝากเงิน', () => {
  it('ฝากเงินเข้าสู่ระบบได้ถูกต้อง', () => {
    const money = 20
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(true)
  })
  it('ฝากเงินเข้าสู่ระบบไม่ได้ Money:0', () => {
    const money = 0
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
  it('ฝากเงินเข้าสู่ระบบไม่ได้ Money:ติดลบ', () => {
    const money = -1
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
  it('ฝากเงินเข้าสู่ระบบไม่ได้ Money:ตัวอักษร', () => {
    const money = 'asx'
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
  it('ฝากเงินเข้าสู่ระบบไม่ได้ Money:null', () => {
    const money = null
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
})
describe('ถอนเงิน', () => {
  it('ถอนเงินออกจากระบบได้ถูกต้อง', () => {
    const moneyUser = 300
    const moneyFrom = 300
    const krapook = krapookService.moneyWithdraw(moneyUser, moneyFrom)
    expect(krapook).toBe(true)
  })
  it('ถอนเงินออกจากระบบไม่ได้ Money:0 ', () => {
    const money = 0
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
  it('ฝากเงินเข้าสู่ระบบไม่ได้ Money:ติดลบ', () => {
    const money = -1
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
  it('ฝากเงินเข้าสู่ระบบไม่ได้ Money:ตัวอักษร', () => {
    const money = 'asx'
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
  it('ฝากเงินเข้าสู่ระบบไม่ได้ Money:null', () => {
    const money = null
    const krapook = krapookService.moneyCheckString(money)
    expect(krapook).toBe(false)
  })
  it('ถอนเงินออกจากระบบมากกว่าเงินที่มี,ถอนเงินออกจากระบบไม่ได้เงินในระบบเป็น 0', () => {
    const moneyUser = 100
    const moneyFrom = 300
    const krapook = krapookService.moneyWithdraw(moneyUser, moneyFrom)
    expect(krapook).toBe(false)
  })
  it('ถอนเงินออกจากระบบไม่ได้เงินในระบบเป็น 0', () => {
    const moneyUser = 0
    const moneyFrom = 300
    const krapook = krapookService.moneyWithdraw(moneyUser, moneyFrom)
    expect(krapook).toBe(false)
  })
})
